//
//  ViewController.swift
//  Puzzler
//
//  Created by Mlg Balrog on 2/12/22.
//  Copyright © 2022 NilosPlace. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var imageView: UIImageView!
    @IBOutlet var takePhotoButton: UIButton!
    @IBOutlet var scanPieces: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        imageView.backgroundColor = .secondarySystemBackground
        takePhotoButton.backgroundColor = .systemBlue
        //button.setTitle("Take Picture", for: .normal)
        takePhotoButton.setTitleColor(.white, for: .normal)
        
        scanPieces.backgroundColor = .systemBlue
        scanPieces.setTitleColor(.white, for: .normal)
        scanPieces.isHidden = true
    }

    @IBAction func takePhoto(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.sourceType = .camera
        picker.delegate = self
        present(picker, animated: true)
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        imageView.image = image
        takePhotoButton.removeFromSuperview()
        scanPieces.isHidden = false
    }
}
